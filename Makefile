NAME     := $(shell python setup.py --name)
VERSION  := $(shell python setup.py --version)
DISTNAME := ${NAME}-${VERSION}.tar.gz

default: dist/${DISTNAME}

dist/${DISTNAME}:
	python setup.py sdist

upload: dist/${DISTNAME}
	twine upload dist/${DISTNAME}

clean:
	python setup.py clean
	rm -rf dist *.egg-info
