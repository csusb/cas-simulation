import logging
import re
import socket
import sys

from .logging_browser import LoggingBrowser

socket.setdefaulttimeout(20.0)

#logging.basicConfig(level=logging.DEBUG)

import mechanize

from functools import wraps

class cas_simulation():
    user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0'

    def handle_http_request(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except mechanize.HTTPError as e:
               logging.error("Received %s %s for %s" % (e.code, e.reason, e.geturl()))
               logging.error("%s", repr(e.read(1024*2)))
               e.seek(0)
               logging.error("%s", repr(str(e.info())))
               raise e
            except mechanize.URLError as e:
               logging.error("Exception during %s: %s" % (f.__name__, e.reason))
               raise e
        return decorated

    def __init__(self, sp_url,
                       cas_user, cas_pass,
                       success_pattern,
                       post_auth_checks=[],skip_form_id=None,
                       user_agent=None):
        self.logger = logging.getLogger('cas_simulation')
        if user_agent:
            self.user_agent = user_agent
        self._init_browser()

        self.sp_url = sp_url
        if not self.sp_url.startswith( ("http://", "https://") ):
            raise ValueError("sp_url is not a url")

        self.cas_user = cas_user
        self.cas_pass = cas_pass
        self.success_pattern = success_pattern
        if not isinstance(self.success_pattern, bytes):
           self.success_pattern = self.success_pattern.encode('utf-8')

        self.post_auth_checks = post_auth_checks
        self.skip_form_id = skip_form_id

    def run(self):
        self._fetch_sp()
        self._verify_login_form()
        response = self._authenticate()
        #raise runtimeerror if no match
        self._check_response(response,self.success_pattern)
        self._do_post_auth_checks()
        #if we made it this far with no exceptions, things must be ok
        return True

    def _init_browser(self):
        self.br = LoggingBrowser()
        # Ignore robots.txt.  Required for Peoplesoft POST
        self.br.set_handle_robots(False)
        self.br.addheaders = [('User-Agent', self.user_agent),
                              ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' )]

    @handle_http_request
    def _fetch_sp(self):
        self.br.open(self.sp_url)
        #Blackboard's SAML pre-login has a form to POST
        self._submit_forms()
        #should end up on a cas logon page
        landing_page = self.br.geturl()

    def _verify_login_form(self):
        landing_page = self.br.geturl()
        self.logger.debug("verifying url as CAS landing page %s", landing_page) 
        if "SAML2/POST/SSO?" in landing_page:
            return
        if "SAML2/Redirect/SSO?" in landing_page:
            return
        if "idp/profile/cas/login?" in landing_page:
            return
        raise RuntimeError("%s redirected to %s, which doesn't look like a CAS logon page" % (self.sp_url, landing_page))

    @handle_http_request
    def _submit_forms(self):
        #sometimes you need to post to the form given by
        #SP after authentication
        try:
            for form in self.br.forms():
                if form.attrs.get('id') == 'csusb-base-ft-fn-hdft-search-form':
                    raise ValueError('form posts back to self at / and breaks simulation')
                if self.skip_form_id is not None:
                    #self.logger.debug("Considering submitting form %s", repr(form.attrs))
                    if form.attrs.get('id') == self.skip_form_id:
                        raise ValueError('stopping simulation at form with id %s' % self.skip_form_id)
                    if form.attrs.get('action') == self.skip_form_id:
                        raise ValueError('stopping simulation at form action %s' % self.skip_form_id)
            self.br.select_form(nr=0)
        except mechanize.BrowserStateError as e:
            #"not viewing HTML"
            r = self.br.response()
            if r:
                self.logger.warning("Bad response %s", repr(r.fp.read()))
            raise e

        except mechanize.FormNotFoundError:
            return None #no secondary post, that's fine
        except ValueError:
            return None
        #except mechanize.BrowserStateError:
            #return None #302 redirects land here?
        #form found, time to post
        self.logger.debug("Submitting post-auth form %s with id %s", repr(form.attrs), form.attrs.get('id'))
        response = self.br.submit()
        return response

    @handle_http_request
    def _authenticate(self):
        try:
           response = None
           for form in self.br.forms():
               self.logger.debug("found form %s", form.attrs['id'])
               if form.attrs['id'] == 'fm1':
                   self.br.form = form
                   break
           if self.br.form is None:
               raise RuntimeError('CAS logon - no form with id "fm1"')
           self.br['j_username'] = self.cas_user
           self.br['j_password'] = self.cas_pass
           response = self.br.submit()
        except RuntimeError as e:
           self.logger.error("Failed Login (runtime): %s", e )
           raise

        for i in range(3):
            second_response = self._submit_forms()
            if second_response:
                response = second_response

        self.logger.debug("Authentication response headers: %s",
                          repr(str(response.info())))
        return response

    def _check_response(self,response,pattern):
        page = response.read()
        if not re.search(pattern,page):
            self.logger.info("WARN: pattern %s not found on %s", repr(pattern), response.geturl())
            self.logger.debug("----- BEGIN %s -----\n", response.geturl())
            self.logger.debug(repr(page))
            self.logger.debug("----- END %s -----", response.geturl())
            raise RuntimeError("pattern %s not found on: %s" % \
                                (repr(pattern), response.geturl()))
        self.logger.info("OK: Pattern %s matches content of: %s", repr(pattern), response.geturl())
        return True

    @handle_http_request
    def _do_post_auth_checks(self):
       for check in self.post_auth_checks:
           response = self.br.open(check['url'])
           if not isinstance(check['pattern'], bytes):
               check['pattern'] = check['pattern'].encode('utf-8')
           if self._check_response(response,check['pattern']):
               logging.info("Success - pattern found on %s",
                   response.geturl())

    @property
    def slowest(self):
        return self.br.slowest

if __name__ == '__main__':
    import getpass
    user = '000226420'
    password = getpass.getpass()
    logging.basicConfig(level=logging.DEBUG)
    for logger in "http_redirects", "http_responses", "http_requests", "equiv":
        l = logging.getLogger("mechanize." + logger)
        l.addHandler(logging.StreamHandler(sys.stderr))
        l.setLevel(logging.DEBUG)

    import sys
    #sp_login('https://weblogon.csusb.edu/cas/login?service=', user, password, '')
    #sp_login('https://csusb.blackboard.com/', user ,password, r"<title>Welcome, .* &ndash; Blackboard Learn</title>")

    s1 = cas_simulation('https://outlook.com/csusb.edu',
                       user ,password,
                       r"<title>Sign in to your account")
    #supposed to send stuff to "mechanize.http_redirects" logger
    s1.br.set_debug_http(True)
    s1.br.set_debug_responses(True)
    s1.br.set_debug_redirects(True)
    s1.run()

    #s2 = cas_simulation('https://cmshr.cms.csusb.edu/HSBPRD/signon.html',
    #                   user, password,
    #                   r"<title>Employee-facing registry content</title>")
    #s2.run()
    #https://weblogon.csusb.edu/cas/login?method=POST&service=https://cmshr.cms.csusb.edu/psp/HSBPRD/?cmd=login%26languageCd=ENG%26userid=PS%26pwd=z
    #https://weblogon.csusb.edu/cas/login?method=post&service=https://cmshr.cms.csusb.edu/psp/hsbprd/?cmd=login%26languagecd=eng%26userid=ps%26pwd=z
    #sp_login('https://weblogon.csusb.edu/cas/login?method=POST&service=https://cmsweb.cms.csusb.edu/psp/HSBPRD/EMPLOYEE/HRMS/h/?tab=DEFAULT%26?cmd=login%26languageCd=ENG%26userid=PS%26pwd=z',
    #                   user ,password, r"<title>Employee-facing registry content</title>")
