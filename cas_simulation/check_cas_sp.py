from __future__ import print_function

import argparse
import logging
import time
import traceback
import sys

import json

import mechanize
import cas_simulation

from urllib.error import URLError, HTTPError
from urllib.request import Request, urlopen

class Nagios:
  OK = 0
  WARNING = 1
  CRITICAL = 2
  UNKNOWN = 3

def check_local(args,credentials,post_auth_checks):
    success = None
    try:
        duration = time.time()
        simulation = cas_simulation.cas_simulation(args.url,
                                                   credentials['username'],credentials['password'],
                                                   args.expression,
                                                   post_auth_checks=post_auth_checks,
                                                   skip_form_id=args.form_id,
                                                   user_agent=args.user_agent)
        success = simulation.run()
        duration = time.time() - duration # wallclock diff as floating point
    except mechanize.HTTPError as e:
        print("CRITICAL %s" % e)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, file=sys.stderr)
        print("Last URL: %s, Reason: %s" % (e.geturl(), e.reason))
        try:
            e.seek(0)
            print("Page Excerpt: %s" % e.read(1024*4))
        except Exception as seek_e:
            print("Page Excerpt (n/a): %s", seek_e)
        sys.exit(Nagios.CRITICAL)
    except mechanize.URLError as e:
        _, _, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, file=sys.stderr)
        print("Simulation Failed: %s" % e.reason)
    except Exception as e:
        _, _, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, file=sys.stderr)
        print("Simulation Failed: %s" % e)

    if not success:
        print("CRITICAL %s failed" % args.url)
        sys.exit(Nagios.CRITICAL)

    if duration > args.warn_after:
        slow_duration,slow_url = simulation.slowest
        print("WARN CAS login for %s took %0.2fs, slowest was %0.2fs for %s" % (args.url,duration,slow_duration,slow_url))
        sys.exit(Nagios.WARNING)

    print("OK CAS login %s authenticated as %s - took %0.2fs" % (args.url, credentials['username'], duration))
    sys.exit(Nagios.OK)

def check_lambda(args,credentials,post_auth_checks,timeout=60):

   import boto3
   import base64

   if args.verbose:
       #with basicConfig(level=DEBUG), we log plain-text passwords.  Not the best thing.
       logging.getLogger('boto3').setLevel(logging.INFO)
       logging.getLogger('botocore').setLevel(logging.INFO)

   payload = { 'url': args.url,
     'username': credentials['username'],
     'password': credentials['password'],
     'expression': args.expression,
     'warn_after': args.warn_after,
     'post_auth_checks': post_auth_checks,
     'skip_form_id': args.form_id,
     'user_agent': args.user_agent
   }

   function_name, region_name, access_key, secret_access_key = args.via_lambda

   client = boto3.client('lambda',
                         aws_access_key_id=access_key,
                         aws_secret_access_key=secret_access_key,
                         region_name=region_name)

   invoke_response = client.invoke(
        FunctionName=function_name,
        InvocationType='RequestResponse',
        Payload=json.dumps(payload),
        LogType = 'Tail'
   )

   response = json.load(invoke_response['Payload'])

#{'ExecutedVersion': '$LATEST',
# 'LogResult': 'U1RBUl...',
# 'Payload': <botocore.response.StreamingBody object at 0x7f7711dcf0f0>,
# 'ResponseMetadata': {'HTTPHeaders': {'connection': 'keep-alive',
#                                      'content-length': '266',
#                                      'content-type': 'application/json',
#                                      'date': 'Tue, 01 Sep 2020 01:38:22 GMT',
#                                      'x-amz-executed-version': '$LATEST',
#                                      'x-amz-log-result': 'U1RBUl...',
#                                      'x-amzn-remapped-content-length': '0',
#                                      'x-amzn-requestid': 'fb9d5406-2d43-49ac-82a3-bd9230833479',
#                                      'x-amzn-trace-id': 'root=1-5f4da607-caf8885c1392a3106ae62b5c;sampled=0'},
#                      'HTTPStatusCode': 200,
#                      'RequestId': 'fb9d5406-2d43-49ac-82a3-bd9230833479',
#                      'RetryAttempts': 0},
# 'StatusCode': 200}

   log_tail = base64.b64decode(invoke_response.get('LogResult'))
   log_tail = log_tail.decode('utf-8',errors='replace')

#b'{"statusCode": 200, "body": "{\\"state\\": \\"WARNING\\", \\"info\\": \\"WARN CAS login for https://weblogon.csusb.edu/cas/login?method=POST&service=https://cmsweb.cms.csusb.edu/psp/CSBPRD/?cmd=login%26languageCd=ENG%26userid=PS%26pwd=z took 6.55s via 34.239.226.26\\\\n\\"}"}'

   state = response.get('state')
   if not state:
       if response.get('error'):
           print(response.get('error'))
       else:
           print(f"CRITICAL: Lambda invoke fail - no 'state' or 'error' in payload.")
           print(response)
       sys.exit(Nagios.CRITICAL)

   logging.debug("%s",log_tail)
   print(response.get('info').strip())
   if state == 'CRITICAL':
      sys.exit(Nagios.CRITICAL)

   if state == 'WARNING':
      sys.exit(Nagios.WARNING)

   if state == 'OK':
      sys.exit(Nagios.OK)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--credentials",
                        help="Location of credentials.json", default="credentials.json")
    parser.add_argument("-s", "--url", required=True,
                        help="The entry point URL, like https://csusb.blackboard.com/")
    parser.add_argument("-e", "--expression", required=True,
                        help="A pattern that should the body of the page upon successful authentication")
    parser.add_argument("-w", "--warn-after", type=float,
                        help="Warn if logon exceeds this time (seconds)", default=5.0)
    parser.add_argument("-v", "--verbose", action='store_true')
    parser.add_argument("--verbose-log", help="Where to log the verbose output", default='/var/log/nagios/cassp-debug.log')
    parser.add_argument("--post-auth-check", nargs=2, metavar=('URL', 'EXPRESSION'),
                        help="Additional post-authentication check")
    parser.add_argument("-f", "--form-id",
                        help="Stop the simulation when a form with this id or action is encountered")
    parser.add_argument("--via-lambda", nargs=4, metavar=("FunctionName", "Region", "AccessKey", "SecretAccessKey"),
                        help="Details for the AWS Lambda function")
    parser.add_argument("--user-agent", default="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0",
                        help="Override the value used in the User-Agent: header")

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(process)d:%(name)s:%(lineno)d]%(levelname)s: %(message)s', filename=args.verbose_log)
    else:
        logging.basicConfig(level=logging.WARNING)

    try:
        with open(args.credentials) as f:
            credentials = json.load(f)
    except IOError as e:
        logging.error("Unable to read '%s'. Create credentials.json or specify --credentials path/to/credentials.json", args.credentials)
        raise e

    post_auth_checks = []
    if args.post_auth_check:
        post_auth_checks.append({'url': args.post_auth_check[0],
                                'pattern': args.post_auth_check[1]})

    if args.via_lambda:
        check_lambda(args,credentials,post_auth_checks)
    else:
        check_local(args,credentials,post_auth_checks)

