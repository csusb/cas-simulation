import logging
import time
import mechanize

class LoggingBrowser(mechanize.Browser):
    def __init__(self, *args, **kwargs):
        self.durations = []
        self.start_times = []
        self.logger = logging.getLogger(__name__)
        return super().__init__(*args, **kwargs)

    def _track_duration(self,duration,request):
        fullurl = request
        if isinstance(fullurl, mechanize.Request):
            fullurl = fullurl.get_full_url()
        self.logger.debug("Completed %0.3fs for %s", duration, fullurl)
        self.durations.append( (duration,fullurl) )

    def open(self, fullurl, *args, **kwargs):
        if isinstance(fullurl, mechanize.Request):
            self.logger.info("Opening %s using %s", fullurl.get_full_url(), fullurl.get_method())
        else:
            self.logger.info("Opening %s", fullurl)

        self.start_times.append(time.time())
        position = len(self.start_times)

        try:
             result = super().open(fullurl,**kwargs)
        except mechanize.URLError as e:
             e.reason = str(e.reason) + " for {}".format(fullurl)
             raise e

        if len(self.start_times) == position:
           self.start_times.append(time.time())
        t2 = self.start_times.pop()
        t1 = self.start_times[-1]
        duration = t2 - t1
        if len(self.start_times) == 1:
           self.start_times.pop()
        self._track_duration(duration,fullurl)
        return result

    def submit(self, *args, **kwargs):
        try:
             method = self.form.method.upper()
             action = self.form.action
             self.logger.info("Submitting %s using %s", action, method)
             return super().submit(*args,**kwargs)
        except mechanize.URLError as e:
             e.reason = str(e.reason) + " during {} for {}".format(method,action)
             raise e
    @property
    def slowest(self):
        if not len(self.durations):
            return None
        return sorted(self.durations, reverse=True)[0]
